package otavio.org.example;

public class Sintatico {
    private Lexico lexico;
    private Token token;
    private int linha;
    private int coluna;

    public void ReconhecerToken(){
        token = lexico.getToken(linha, coluna);
        coluna = token.getColuna() + token.getTamanhoToken();
        linha = token.getLinha();
        System.out.println(token);
    }

    public Sintatico(String nomeArquivo){
        linha = 1;
        coluna = 1;
        this.lexico = new Lexico(nomeArquivo);
    }

    public void Reconhecer()
    {
        ReconhecerToken();
        programa();
    }

    public void mensagemErro(String msg) {
        System.err.println("Linha: " + token.getLinha() +
                ", Coluna: " + token.getColuna() +
                msg);
    }

    public void programa() {
        if ((token.getClasse() == Classe.cPalRes)
                && (token.getValor().getValorIdentificador().equalsIgnoreCase("program"))) {
            ReconhecerToken();
            if (token.getClasse() == Classe.cId) {
                ReconhecerToken();
                corpo();
                if (token.getClasse() == Classe.cPonto) {
                    ReconhecerToken();
                } else {
                    mensagemErro(" -> não está encerrando com ponto '.'");
                }
            } else {
                mensagemErro(" -> o programa não possui nome");
            }
        } else {
            mensagemErro(" -> não está começando com 'Program'");
        }
    }

    public void corpo() {
        declara();
        if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("begin"))) {
            ReconhecerToken();
            sentencas();
            if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("end"))) {
                ReconhecerToken();
            }else {
                mensagemErro(" -> não está encerrando com 'End'");
            }
        }else {
            mensagemErro(" -> não possui 'Begin' no corpo do programa");
        }
    }

    public void declara() {
        if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("var"))) {
            ReconhecerToken();
            dvar();
            mais_dc();
        }
    }

    public void mais_dc() {
        if (token.getClasse() == Classe.cPontoVirgula) {
            ReconhecerToken();
            cont_dc();
        } else {
            mensagemErro(" -> a instrução não possui ponto e vírgula ';'");
        }
    }

    public void cont_dc() {
        if (token.getClasse() == Classe.cId) {
            dvar();
            mais_dc();
        }
    }

    public void dvar() {
        variaveis();
        if (token.getClasse() == Classe.cDoisPontos) {
            ReconhecerToken();
            tipo_var();
        }else {
            mensagemErro(" -> a instrução não possui dois pontos ':'");
        }
    }

    public void tipo_var() {
        if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("integer"))) {
            ReconhecerToken();
        }else if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("real"))) {
            ReconhecerToken();
        }else {
            mensagemErro(" -> o tipo Integer não está sendo declarado");
        }
    }

    public void variaveis() {
        if (token.getClasse() == Classe.cId) {
            ReconhecerToken();
            mais_var();
        }else {
            mensagemErro(" -> não possui identificador");
        }
    }

    public void mais_var(){
        if (token.getClasse() == Classe.cVirgula) {
            ReconhecerToken();
            variaveis();
        }
    }


    public void sentencas() {
        comando();
        mais_sentencas();
    }


    public void mais_sentencas() {
        if (token.getClasse() == Classe.cPontoVirgula) {
            ReconhecerToken();
            cont_sentencas();
        }else {
            mensagemErro(" -> a instrução não possui ponto e vírgula ';'");
        }
    }



    public void cont_sentencas() {
        if (((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("read"))) ||
                ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("write"))) ||
                ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("for"))) ||
                ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("repeat"))) ||
                ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("while"))) ||
                ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("if"))) ||
                ((token.getClasse() == Classe.cId))
        ) {
            sentencas();
        }
    }


    public void var_read() {
        if (token.getClasse() == Classe.cId) {
            ReconhecerToken();
            mais_var_read();
        }else {
            mensagemErro(" -> não possui identificador");
        }
    }

    public void mais_var_read() {
        if (token.getClasse() == Classe.cVirgula) {
            ReconhecerToken();
            var_read();
        }
    }


    public void var_write() {
        if (token.getClasse() == Classe.cId) {
            ReconhecerToken();
            mais_var_write();
        }else {
            mensagemErro(" -> não possui identificador");
        }
    }

    public void mais_var_write() {
        if (token.getClasse() == Classe.cVirgula) {
            ReconhecerToken();
            var_write();
        }
    }

    public void comando() {

        if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("read"))){
            ReconhecerToken();
            if (token.getClasse() == Classe.cParEsq) {
                ReconhecerToken();
                var_read();
                if (token.getClasse() == Classe.cParDir) {
                    ReconhecerToken();
                }else {
                    mensagemErro(" -> não possui parêntese direito ')'");
                }
            }else {
                mensagemErro(" -> não possui parêntese esquerdo '('");
            }
        }else
            if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("write"))){
                ReconhecerToken();
                if (token.getClasse() == Classe.cParEsq) {
                    ReconhecerToken();
                    var_write();
                    if (token.getClasse() == Classe.cParDir) {
                        ReconhecerToken();
                    }else {
                        mensagemErro(" -> não possui parêntese direito ')'");
                    }
                }else {
                    mensagemErro(" -> não possui parêntese esquerdo '('");
                }
            }else

            if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("for"))){
                ReconhecerToken();
                if (token.getClasse() == Classe.cId) {
                    ReconhecerToken();

                    if (token.getClasse() == Classe.cAtribuicao){
                        ReconhecerToken();
                        expressao();

                        if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("to"))){
                            ReconhecerToken();
                            expressao();

                            if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("do"))){
                                ReconhecerToken();
                                if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("begin"))){
                                    ReconhecerToken();
                                    sentencas();
                                    if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("end"))){
                                        ReconhecerToken();
                                    }else {
                                        mensagemErro(" -> não possui 'End' no For");
                                    }
                                }else {
                                    mensagemErro(" -> não possui 'Begin' no For");
                                }
                            }else {
                                mensagemErro(" -> não possui 'Do' no For");
                            }
                        }else {
                            mensagemErro(" -> não possui 'To' no For");
                        }
                    }else {
                        mensagemErro(" -> não possui ':=' no For");
                    }
                }else {
                    mensagemErro(" -> não possui identificador no início do For");
                }
            }else

            if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("repeat"))){
                ReconhecerToken();
                sentencas();

                if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("until"))){
                    ReconhecerToken();
                    if (token.getClasse() == Classe.cParEsq){
                        ReconhecerToken();
                        condicao();
                        if (token.getClasse() == Classe.cParDir){
                            ReconhecerToken();
                        }else {
                            mensagemErro(" -> não fechou parênteses no Repeat");
                        }
                    }else {
                        mensagemErro(" -> não abriu parênteses no Repeat");
                    }
                }else {
                    mensagemErro(" -> não possui 'Until' no Repeat");
                }
            }

            else if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("while"))){
                ReconhecerToken();
                if (token.getClasse() == Classe.cParEsq){
                    ReconhecerToken();
                    condicao();
                    if (token.getClasse() == Classe.cParDir){
                        ReconhecerToken();
                        if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("do"))){
                            ReconhecerToken();
                            if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("begin"))){
                                ReconhecerToken();
                                sentencas();
                                if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("end"))){
                                    ReconhecerToken();
                                }else {
                                    mensagemErro(" -> não possui 'End' no While");
                                }
                            }else {
                                mensagemErro(" -> não possui 'Begin' no While");
                            }
                        }else {
                            mensagemErro(" -> não possui 'Do' no While");
                        }
                    }else {
                        mensagemErro(" -> não possui ')' no While");
                    }
                }else {
                    mensagemErro(" -> não possui '(' no While");
                }
            }
            else if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("if"))){
                ReconhecerToken();
                if (token.getClasse() == Classe.cParEsq){
                    ReconhecerToken();
                    condicao();
                    if (token.getClasse() == Classe.cParDir){
                        ReconhecerToken();

                        if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("then"))){
                            ReconhecerToken();
                            if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("begin"))){
                                ReconhecerToken();
                                sentencas();
                                if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("end"))){
                                    ReconhecerToken();
                                    pfalsa();
                                }else {
                                    mensagemErro(" -> não possui 'End' no While");
                                }
                            }else {
                                mensagemErro(" -> não possui 'Begin' no While");
                            }
                        }else {
                            mensagemErro(" -> não possui 'Do' no While");
                        }
                    }else {
                        mensagemErro(" -> não possui ')' no While");
                    }
                }else {
                    mensagemErro(" -> não possui '(' no While");
                }
            }
            else if (token.getClasse() == Classe.cId){
                ReconhecerToken();

                if (token.getClasse() == Classe.cAtribuicao){
                    ReconhecerToken();
                    expressao();
                }
                else {
                    mensagemErro(" -> não possui atribuição");
                }
            }
    }

    public void condicao() {
        expressao();
        relacao();
        expressao();
    }


    public void pfalsa() {
        if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("else"))){
            ReconhecerToken();
            if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("begin"))){
                ReconhecerToken();
                sentencas();
                if ((token.getClasse() == Classe.cPalRes) && (token.getValor().getValorIdentificador().equalsIgnoreCase("end"))){
                    ReconhecerToken();
                }else {
                    mensagemErro(" -> não está encerrando com 'End'");
                }
            }else {
                mensagemErro(" -> não está começando com 'Begin'");
            }
        }
    }

    public void relacao() {
        if (token.getClasse() == Classe.cIgual) {
            ReconhecerToken();
        }else if (token.getClasse() == Classe.cMaior) {
            ReconhecerToken();
        }else if (token.getClasse() == Classe.cMenor) {
            ReconhecerToken();
        }else if (token.getClasse() == Classe.cMaiorIgual) {
            ReconhecerToken();
        }else if (token.getClasse() == Classe.cMenorIgual) {
            ReconhecerToken();
        }else if (token.getClasse() == Classe.cDiferente) {
            ReconhecerToken();
        }else {
            mensagemErro(" -> não possui o operador de relação");
        }
    }

    public void expressao() {
        termo();
        outros_termos();
    }

    public void outros_termos() {
        if (token.getClasse() == Classe.cMais || token.getClasse() == Classe.cMenos) {
            op_ad();
            termo();
            outros_termos();
        }
    }

    public void op_ad() {
        if (token.getClasse() == Classe.cMais || token.getClasse() == Classe.cMenos) {
            ReconhecerToken();
        }else {
            mensagemErro(" -> não possui o operador de adição ou subtração");
        }
    }

    public void termo() {
        fator();
        mais_fatores();
    }


    public void mais_fatores() {
        if (token.getClasse() == Classe.cMultiplicacao || token.getClasse() == Classe.cDivisao) {
            op_mul();
            fator();
            mais_fatores();
        }
    }

    public void op_mul() {
        if (token.getClasse() == Classe.cMultiplicacao || token.getClasse() == Classe.cDivisao) {
            ReconhecerToken();
        }else {
            mensagemErro(" -> não possui o operador de multiplicação ou divisão");
        }
    }
    
    public void fator() {
        if (token.getClasse() == Classe.cId) {
            ReconhecerToken();
        }else if (token.getClasse() == Classe.cInt || token.getClasse() == Classe.cReal) {
            ReconhecerToken();
        }else if (token.getClasse() == Classe.cParEsq){
            ReconhecerToken();
            expressao();
            if (token.getClasse() == Classe.cParDir){
                ReconhecerToken();
            }else {
                mensagemErro(" -> não possui parêntese direito ')'");
            }
        }else {
            mensagemErro(" -> não possui fator 'In Num Exp'");
        }
    }
}
